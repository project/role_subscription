<?php

/**
 * @file
 * Handles the database functions for managing user subscription history
 */

/**
 * Functions for managing user subscription history 
 */
class User_subscription_history_db {
	
	/**
	 * Add user subscription payment version for user subscription history tracking
	 * 
	 * @param array $payment_info
	 * @param integer $user_payment_id
	 * @param integer $role_subscription_id
	 * @param string $status defaults to 'active'
	 * 
	 * @return mixed a database query result resource, or FALSE if the query was not executed correctly.
	 */
	function add_version($payment_info, $user_payment_id, $role_subscription_id, $status = 'active'){
		$query = "insert into {role_subscription_user_payments_history} " .
				 "(user_payment_id, role_subscription_id, first_name, last_name, company_name, address1, address2, " .
				 "city, state, zip, phone, status) " .
			     "values ('%d', '%d', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s');";
			   
		return db_query($query, $user_payment_id, $role_subscription_id, 
								$payment_info['first_name'], $payment_info['last_name'], 
								$payment_info['company_name'], $payment_info['address1'], 
								$payment_info['address2'], $payment_info['city'], $payment_info['state'], 
								$payment_info['zip'], $payment_info['phone'], $status);//will return 1 if success
	}
	
	/**
	 * Find version ID
	 * 
	 * @param integer $vid
	 * 
	 * @return object user subscription version data
	 */
	function find($vid){
		$query = "select rsph.* from {role_subscription_user_payments_history} rsph where rsph.vid = %d; ";
			   
		return db_fetch_object(db_query($query, $vid));
	}
	
	/**
	 * Find user subscription history for a user subscription id
	 * 
	 * @param integer $user_payment_id user subscription id
	 * 
	 * @return mixed a database query result resource, or FALSE if the query was not executed correctly. 
	 */
	function find_all($user_payment_id){
		if (!$user_payment_id) return;//fail
		
		$query = "select rsph.*, rsh.title " .
				 "from {role_subscription_user_payments_history} rsph " .
				 "join {role_subscription} rs on rsph.role_subscription_id = rs.id " .
				 "join {role_subscription_history} rsh on rsh.vid = rs.vid " .
				 "where rsph.user_payment_id = %d order by created_at desc; ";
			   
		return db_query($query, $user_payment_id);
	}
}