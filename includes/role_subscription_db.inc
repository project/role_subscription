<?php

/**
 * @file
 * Handles the database functions for managing subscriptions and subscription groups
 */

/**
 * Functions for managing subscriptions and subscription groups
 */
class Role_subscription_db {
	/**
	 * Check if a subscription has changed
	 * 
	 * @param array $form_values form values for role subscription
	 * @param integer $id role subscription ID
	 * @return bool TRUE or FALSE
	 */
	function subscription_has_changed($form_values, $id){
		if (!$id) return FALSE;//no id error
		if (!$subscription = Role_subscription_db::find($id)) return FALSE;//no record found
		
		$record_changed = FALSE;
		
		$subscription_cols = array('title', 'price', 'trial_amount', 'total_occurrences',
								   'trial_occurrences', 'description', 'weight');//cols that will be updated

		/**
		 * Go through each subscription field and compare form values with the db values
		 * to see if the subscription has changed
		 */
		foreach ($subscription_cols as $col)
			if ($form_values[$col] != $subscription->{$col})
				$record_changed = TRUE;//if anything gets changed we have to resubmit
				
		return $record_changed;
	}
	
	/**
	 * Add role subscription
	 * 
	 * @param array $form_values form values to be inserted
	 * 
	 * @return mixed a database query result resource, or FALSE if the query was not executed correctly.
	 */
	function add_subscription($form_values){
		$f = $form_values;//shortcut var
		
		$query = "insert into {role_subscription} (vid, rid, role_subscription_group_id) " .
			     "values ('%d', '%d', '%d');";
		
		$id = db_next_id('role_subscription');//get next id for this table	   
		$vid = db_next_id('role_subscription_history');//get next id for this table
		
		if (!$id OR !$vid) return;//fail
		
		$result = db_query($query, $vid, $f['rid'], $f['role_subscription_group_id']);
			
		/**
		 * Add version
		 */					
		if ($result)
			Role_subscription_history_db::add_version($f, $id, 'active');//pass form values and new role subscription ID
		
		return $result;
	}
	
	
	/**
	 * Edit subscription
	 * 
	 * @param array $form_values form values to be updated
	 * @param integer $id role subscription ID
	 * 
	 * @return mixed a database query result resource, or FALSE if the query was not executed correctly.
	 */
	function edit_subscription($form_values, $id) {
		if (!$id) return;//no id error

		$vid = db_next_id('role_subscription_history');//get next id for this table
		
		/**
		 * Add a new version
		 */
		if (!Role_subscription_history_db::add_version($form_values, $id, 'active'))
			return;//fail
		
		/**
		 * Update role subscription to point to new version
		 */
		$query = "update {role_subscription} set vid = '%d' where id = %d;";
			   
		return db_query($query, $vid, $id);
	}
		
	/**
	 * Find role subscription for an ID
	 * 
	 * @return object role subscription data
	 */
	function find($id) {
		if (!$id) return;
	
		$query = 'select rsh.*, rs.id, rs.rid, rs.role_subscription_group_id, r.name as role, rsg.name as group_name, ' .
				 'rsg.unit, rsg.length from {role_subscription} rs ' .
				 'join {role_subscription_history} rsh on rsh.vid = rs.vid ' .
				 'join {role_subscription_group} rsg on rs.role_subscription_group_id = rsg.id ' .
				 'join {role} r on r.rid = rs.rid where rs.id = "%d" ' .
				 'and rsh.status = "active" and rsg.status = "active"; ';
		
		return db_fetch_object(db_query($query, $id));	
	}
	
	/**
	 * Find all subscriptions
	 * 
	 * @return mixed a database query result resource, or FALSE if the query was not executed correctly.
	 */
	function find_all(){
		$query = 'select rsh.*, rs.id, rs.rid, rs.role_subscription_group_id, r.name as role, rsg.name as group_name, ' .
				 'rsg.unit, rsg.length from {role_subscription} rs ' .
				 'join {role_subscription_history} rsh on rsh.vid = rs.vid ' .
				 'join {role} r on r.rid = rs.rid ' .
				 'join {role_subscription_group} rsg on rs.role_subscription_group_id = rsg.id ' .
				 'where rsh.status = "active" and rsg.status = "active" order by rsh.weight asc; ';
		
		return db_query($query);
	}
	
	/**
	 * Group role subscriptions by subscription groups
	 * 
	 * @return array associate array where the group name is the hash key which has an array of subscriptions as it's value
	 */
	function find_all_in_groups(){
		$result = self::find_all();//get db result
		
		while ($row = db_fetch_object($result))
			$groups [$row->group_name][] = $row;
		
		return $groups;
	}
	
	/**
	 * Get the count of subscriptions for a group ID
	 * @param integer $id group ID
	 * 
	 * @return integer count of subscriptions
	 */
	function subscriptions_count_for_group($id){
		if (!$id) return;//fail
		
		$query = 'select count(*) as count from {role_subscription} rs ' .
				 'join {role_subscription_history} rsh on rsh.vid = rs.vid ' .
				 'where rsh.status = "active" and role_subscription_group_id = %d';
		
		return db_fetch_object(db_query($query, $id))->count;
	}
	
	/**
	 * Cancel a role subscription in the database (will not delete but hide for reporting)
	 * 
	 * @param $id role subscription ID
	 * 
	 * @return mixed a database query result resource, or FALSE if the query was not executed correctly.
	 */
	function cancel($id){
		$sub = (array) Role_subscription_db::find($id);//get role subscription info
		
		$vid = db_next_id('role_subscription_history');//get next id for this table
		
		/**
		 * Add a new version as "cancelled"
		 */
		if (!Role_subscription_history_db::add_version($sub, $id, 'cancelled'))
			return;//fail
		
		/**
		 * Update role subscription to point to new version
		 */
		$query = "update {role_subscription} set vid = '%d' where id = %d;";
			   
		return db_query($query, $vid, $id);
	}
	
	/**
	 * Add a role subscription group
	 * 
	 * Unit and length cannot be changed once a group is created.
	 * 
	 * @param array $group_info hash of group form data
	 *   - "name": name of group container
	 *   - "description"
	 *   - "unit": 'months' or 'days' will be used for all role subscriptions in this container
	 *   - "length": 1-12 for months or 7-365 for days
	 * 
	 * @return mixed a database query result resource, or FALSE if the query was not executed correctly.
	 */
	function add_group($group_info){
		$query = 'insert into {role_subscription_group} (name, description, unit, length) values ("%s", "%s", "%s", "%d");';
		
		return db_query($query, $group_info['name'], $group_info['description'], $group_info['unit'], $group_info['length']);
	}
	
	/**
	 * Edit role subscription group
	 * 
	 * Note that this will not update unit or length. This cannot be changed for a group container. It has to be deleted
	 * for these values to be changed.
	 * 
	 * @param array $group_info hash of group form data
	 * @param integer $id role subscription group ID
	 * 
	 * @return mixed a database query result resource, or FALSE if the query was not executed correctly.
	 */
	function edit_group($group_info, $id){
		if (!$id) return;//fail
		
		$query = 'update {role_subscription_group} set name = "%s", description = "%s" where id = "%d";';
		
		return db_query($query, $group_info['name'], $group_info['description'], $id);
	}
	
	/**
	 * Delete subscription group
	 * 
	 * @param integer $id subscription group ID
	 * 
	 * @return mixed a database query result resource, or FALSE if the query was not executed correctly.
	 */
	function delete_group($id) {
		if (!$id) return;//fail
		
		$query = 'update {role_subscription_group} set status = "deleted" where id = "%d";';//actually just hides the group
		
		return db_query($query, $id);
	}
	
	/**
	 * Find all subscription groups
	 * 
	 * @return mixed a database query result resource, or FALSE if the query was not executed correctly.
	 */
	function find_all_groups(){
		$query = 'select rsg.* from {role_subscription_group} rsg where rsg.status = "active"; ';
		
		return db_query($query);
	}
	
	/**
	 * Get a count of existing subscription groups
	 * 
	 * @return integer count of how many subscription groups exist
	 */
	function get_groups_count(){
		$query = 'select count(*) as count from {role_subscription_group} rsg where rsg.status = "active"; ';
		
		return db_fetch_object(db_query($query))->count;
	}
	
	/**
	 * Get all role subscriptions for a group in an options hash
	 * 
	 * @param integer $role_subscription_group_id group ID
	 * 
	 * @return array role subscriptions for a particular group
	 */
	function get_subscription_options_for_group($role_subscription_group_id){
		if (!$role_subscription_group_id) return;//fail
		
		$query = 'select rsh.*, rs.id, rs.rid, rs.role_subscription_group_id from {role_subscription} rs ' .
				 'join {role_subscription_history} rsh on rsh.vid = rs.vid ' .
				 'where rsh.status = "active" AND rs.role_subscription_group_id = "%d";';
		
		$result = db_query($query, $role_subscription_group_id);
		
		while ($row = db_fetch_object($result))
			$options [$row->id] = $row->title;

		return $options;
	}
	
	/**
	 * Find group
	 * 
	 * @param integer $id subscription group ID
	 * 
	 * @return object group data
	 */
	function find_group($id){
		if (!$id) return;//fail
		
		$query = 'select rsg.* from {role_subscription_group} rsg where id = %d and rsg.status = "active"';
		
		return db_fetch_object(db_query($query, $id));
	}
	
	/**
	 * Get dropdown array of groups
	 * 
	 * @return array groups as options array 
	 */
	function groups_dropdown(){
		$result = self::find_all_groups();//get db result
		
		$groups [''] = '';
		
		while ($row = db_fetch_object($result))
			$groups [$row->id] = $row->name;
		
		return $groups;
	}
	
	/**
	 * Find all user roles
	 * 
	 * @todo is there not a drupal function for this?
	 * 
	 * @return mixed a database query result resource, or FALSE if the query was not executed correctly. 
	 */
	function find_all_roles(){
		return db_query('select * from {role} r');		
	}
	
	/**
	 * Function find all roles as options array
	 * 
	 * @todo is there not a drupal function for this?
	 * 
	 * @return array roles as options array
	 */
	function roles_dropdown(){
		$result = self::find_all_roles();
		
		$roles [''] = '';
		
		while ($row = db_fetch_object($result))
			$roles [$row->rid] = $row->name;
			
		return $roles;
	}
}