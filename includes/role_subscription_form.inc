<?php

/**
 * Helper class for role subscription module, all methods called statically, acts as namespace
 */
class Role_subscription_form {
	/**
	 * Helper function for role subscription add and edit forms
	 * @param integer $form assoc array of form values
	 * @param string $edit_mode either 'add' or 'update'
	 * 
	 * @return mixed assoc array of form items
	 */
	function get_form($form_values = NULL, $edit_mode = 'add'){
		
		$form['title'] = array(
		    '#title' => t('Subscription Title'),
			'#type' => 'textfield',
		    '#size' => 60,
		    '#maxlength' => 255,
		    '#default_value' => $form_values['title'],
		    '#description' => 'Choose a human readable title for this subscription.',
		    '#required' => TRUE 
		);
		
		if ($edit_mode == 'add') {
			$form['rid'] = array(
			    '#type' => 'select',
			    '#title' => t('Role'),
			    '#options' => Role_subscription_db::roles_dropdown(),
			    '#default_value' => $form_values['rid'],
			    '#description' => 'Choose the role that this subscription will grant, '.l('edit roles here', 'admin/user/roles').'.',
			    '#required' => TRUE
			);
		}
		/**
		 * Can't modify role once subscription is created 
		 */
		else {
			$form['role'] = array(
			    '#type' => 'item',
			    '#title' => t('Role'),
			    '#value' => t($form_values['role']),
			    '#description' => 'This role cannot be modified.',
			);
		}
		
		if ($edit_mode == 'add') {
			$form['role_subscription_group_id'] = array(
			    '#type' => 'select',
			    '#title' => t('Subscription Group'),
			    '#options' => Role_subscription_db::groups_dropdown(),
			    '#default_value' => $form_values['role_subscription_group_id'],
			    '#description' => 'Choose subscription group for this subscription role.',
			    '#required' => TRUE
			);
		}
		/**
		 * Can't modify group once role subscription is created
		 */
		else {
			$form['group'] = array(
			    '#type' => 'item',
			    '#title' => t('Subscription Group'),
			    '#value' => $form_values['group_name'],
			    '#description' => 'This group cannot be modified.'
			);
		}
		
		$form['price'] = array(
		    '#title' => t('Subscription Price'),
			'#type' => 'textfield',
		    '#size' => 10,
		    '#maxlength' => 10,
		    '#default_value' => $form_values['price'],
		    '#description' => 'The price of the subscription after it\'s initial trial period (if any).',
		    '#required' => TRUE
		);
		
		$form['trial_amount'] = array(
		    '#title' => t('Trial Amount'),
			'#type' => 'textfield',
		    '#size' => 10,
		    '#maxlength' => 10,
		    '#default_value' => $form_values['trial_amount'],
		    '#description' => 'The price of the subscription during it\'s trial period, if applicable.'
		);
		
		$form['total_occurrences'] = array(
		    '#title' => t('Total Occurrences'),
			'#type' => 'textfield',
		    '#size' => 3,
		    '#maxlength' => 3,
		    '#default_value' => $form_values['total_occurrences'],
		    '#description' => 'Choose how many times the subscription will be billed. ' .
		    				  'Leave blank if subscription is recurring until the user cancels.'
		);
		
		$form['trial_occurrences'] = array(
		    '#title' => t('Trial Occurrences'),
			'#type' => 'textfield',
		    '#size' => 3,
		    '#maxlength' => 3,
		    '#default_value' => $form_values['trial_occurrences'],
		    '#description' => 'Choose how many times the subscription will be billed at the trial amount, if applicable. '
		);
		
		$form['description'] = array(
		    '#title' => t('Description'),
			'#type' => 'textarea',
		    '#default_value' => $form_values['description'],
		    '#description' => 'Describe the subscription. This can include business rules set up by other modules. This can include html markup.',
		    '#required' => TRUE
		);
		
		$form['weight'] = array(
		    '#title' => t('Weight'),
			'#type' => 'select',
		    '#options' => array_combine(range(-10, 10), range(-10, 10)),
		    '#default_value' => $form_values['weight'] ? $form_values['weight'] : 0,
		    '#description' => 'Determine the order of the subscription when it is displayed. '
		);
		
		if ($edit_mode == 'edit')
			$form['force_update'] = array(
			    '#title' => t('Force Merchant Update?'),
				'#type' => 'checkbox',
			    '#default_value' => FALSE,
			    '#description' => "This will force merchant API update calls even if the role subscription hasn't changed. This is" .
			    		"helpful if the merchant API calls timed out when the role subscription was updated. Forcing a merchant update " .
			    		"will sync the merchant details with the local database. "
			);
		
		$form['submit'] = array(
			'#type' => 'submit',
			'#value' => $edit_mode == 'add' ? t('Add Role Subscription') : t('Update Role Subscription'),
		);
		
		/**
		 * Special instructions for admin updating subscription
		 */
		if ($edit_mode == 'edit')
			$form['#attributes'] = array(
				'onSubmit' => 'return confirm(\'Are you sure you want to modify this subscription? This will update all user' .
						'subscriptions tied to this role subscription.\');'
			);
		
		return $form;
	}
	
	/**
	 * Common function for validating add and update subscripion forms
	 * 
	 * @return mixed
	 */
	function get_validate_form($form_id, $form_values, $edit_mode = 'add'){
		/**
		 * Title
		 */
		if (strlen($form_values['title']) > 255)
			form_set_error('title', t('The Subscription Title cannot be longer than 255 characters.'));
		
		/**
		 * Extra fields to validate if add mode
		 */
		if ($edit_mode == 'add') {
			/**
			 * User Role
			 */
			if (!ctype_digit($form_values['rid']))
				form_set_error('rid', t('The Role you submitted is invalid, the value must be an integer.'));//this should never trigger
			
			/**
			 * Subscription Group
			 */
			if (!ctype_digit($form_values['role_subscription_group_id']))
				form_set_error('role_subscription_group_id', t('The Subscription Group you submitted is invalid, the value must be an integer.'));//this should never trigger
		}	
		
		/**
		 * Subscription Price
		 */
		if (!is_numeric($form_values['price']))
			form_set_error('price', t('The Price must be a valid numeric value only.'));
			
		/**
		 * Trial Amount
		 */
		if ($form_values['trial_amount']) {
			if (!is_numeric($form_values['trial_amount']))
				form_set_error('trial_amount', t('The Trial Amount must be a valid numeric value only.'));
		}
		
		/**
		 * Total Occurrences
		 */
		if ($form_values['total_occurrences']) {
			if (!ctype_digit($form_values['total_occurrences']))
				form_set_error('total_occurrences', t('The Total Occurrences value must be a valid number greater than zero only or left blank.'));
		}
		
		/**
		 * Trial Occurrences
		 */
		if ($form_values['trial_occurrences']) {
			if (!ctype_digit($form_values['trial_occurrences']))
				form_set_error('trial_occurrences', t('The Trial Occcurrences value must be a valid number greater than zero only or left blank.'));
		}
		
		/**
		 * Weight
		 */
		if (!is_numeric($form_values['weight']))
			form_set_error('weight', t('Weight value must be a positive or negative number.'));
	}
	
	/**
	 * Return payment form items
	 */
	function get_payment_form($form_values=NULL, $edit_mode, $sub=NULL){
		/**
		 * Subscription info (only show when user first subscribes)
		 */
		if ($sub) {
			$form['terms'] = array(
				'#type' => 'fieldset',
				'#collapsible' => TRUE,
				'#title' => t('Terms for '.$sub->title),
				'#description' => $sub->description,
				'#attributes' => array('id' => 'role_subscription_pay_terms')
			);
			
			$form['terms']['subscription_terms'] = array(
				'#value' => self::get_subscription_info_html_table($sub),
			);
		}

		//billing info
		$form['billing_info'] = array(
		    '#type' => 'fieldset',
		    '#title' => t('Billing information'),
		    '#collapsible' => TRUE,
		);
		
		$form['billing_info']['first_name'] = array(
		    '#title' => t('First Name'),
			'#type' => 'textfield',
		    '#size' => 25,
		    '#maxlength' => 25,
		    '#default_value' => $form_values['first_name'],
		    '#required' => TRUE
		);
		
		$form['billing_info']['last_name'] = array(
		    '#title' => t('Last Name'),
			'#type' => 'textfield',
		    '#size' => 25,
		    '#maxlength' => 25,
		    '#default_value' => $form_values['last_name'],
		    '#required' => TRUE
		);
		
		$form['billing_info']['company_name'] = array(
		    '#title' => t('Company Name'),
			'#type' => 'textfield',
		    '#size' => 25,
		    '#maxlength' => 25,
		    '#default_value' => $form_values['company_name']
		);
		
		$form['billing_info']['address1'] = array(
		    '#title' => t('Street Address'),
			'#type' => 'textfield',
		    '#size' => 25,
		    '#maxlength' => 25,
		    '#default_value' => $form_values['address1'],
		    '#required' => TRUE
		);
		
		$form['billing_info']['address2'] = array(
			'#type' => 'textfield',
		    '#size' => 25,
		    '#maxlength' => 25,
		    '#default_value' => $form_values['address2']
		);
		
		$form['billing_info']['city'] = array(
		    '#title' => t('City'),
			'#type' => 'textfield',
		    '#size' => 25,
		    '#maxlength' => 25,
		    '#default_value' => $form_values['city'],
		    '#required' => TRUE
		);
		
		$form['billing_info']['state'] = array(
		    '#type' => 'select',
		    '#title' => t('State'),
		    '#options' => array('' => '') + location_province_list_us(),//location module required, add blank
		    '#default_value' => $form_values['state'],
		    '#required' => TRUE 
		);
		
		$form['billing_info']['zip'] = array(
		    '#title' => t('Zip Code'),
			'#type' => 'textfield',
		    '#size' => 10,
		    '#maxlength' => 10,
		    '#default_value' => $form_values['zip'],
		    '#required' => TRUE,
		    '#description' => 'Valid format is 5 digit, e.g. xxxxx, or 5 digit plus 4, e.g. xxxxx-xxxx'
		);
		
		$form['billing_info']['phone'] = array(
		    '#title' => t('Phone'),
			'#type' => 'textfield',
		    '#size' => 25,
		    '#maxlength' => 25,
		    '#default_value' => $form_values['phone'],
		    '#required' => TRUE,
		    '#description' => 'Valid format is xxx-xxx-xxxx (extensions ok)'
		);
		
		//payment info
		$form['payment_info'] = array(
		    '#type' => 'fieldset',
		    '#title' => t('Payment method'),
		    '#collapsible' => TRUE,
		    '#description' => '<p>Your billing information must match the billing address for the credit card entered below or we will be unable to process your payment.</p>',
		);
		
		$form['payment_info']['cc_type'] = array(
		    '#type' => 'radio',
		    '#title' => t('Credit card: ').sprintf('<img src="%s" style="vertical-align:middle;" alt="Visa Mastercard American Express Discover" width="179" height="30" />', url().drupal_get_path('module', 'role_subscription') .'/images/credit_card_logos_43.gif'),
		    '#default_value' =>  '',
		    '#attributes' => array('disabled' => 'disabled')
		);
		
		$form['payment_info']['cc_number'] = array(
		    '#title' => t('Credit Card Number'),
			'#type' => 'textfield',
		    '#size' => 16,
		    '#maxlength' => 16,
		    '#default_value' => $form_values['cc_number'],
		    '#required' => TRUE,
		    '#description' => 'Typically 15 or 16 numbers, digits only, no spaces.'
		);
		
		$form['payment_info']['expiration_date'] = array(
		    '#title' => t('Expiration Date'),
			'#type' => 'textfield',
		    '#size' => 7,
		    '#maxlength' => 7,
		    '#default_value' => $form_values['expiration_date'],
		    '#description' => 'Give the month and year of your card\'s expiration date, e.g. mm/yyyy',
		    '#required' => TRUE
		);
		
		$form['payment_info']['privacy'] = array(
		    '#value' => 
					'<div>*We do not save credit card information.</div> ' .
		    		'<div><a href="'.url('role_subscription/privacy').'" ' .
		    		'onClick="window.open(\''.url('role_subscription/privacy').'\', \'Privacy Information\', \'height=300,width=400\'); return false;">' .
		    		'Read more about privacy</a> <em>(new window)</em>.</div>' 
		);
		
		/**
		 * Show different buttons based on edit mode
		 */
		if ($edit_mode == 'add') {
			$form['cancel'] = array(
				'#value' => sprintf('<input type="button" value="Cancel" class="cancel" onClick="window.location=\'%s\';" style="margin-right:8px;">', url('role_subscription')),
			);
			
			$form['submit'] = array(
				'#type' => 'submit',
				'#value' => 'Review Order'
			);
		}
		else {
			$form['submit'] = array(
				'#type' => 'submit',
				'#value' => 'Update Payment'
			);
		}
		
		return $form;
	}
	
	/**
	 * Return dummy values for payment screen
	 */
	function get_dummy_payment_form_data(){
		return array(
			'first_name'	=> 'John',
			'last_name'		=> 'Doe',
			'address1'		=> '1234 Rich Man Drive',
			'city'			=> 'Little Rock',
			'state'			=> 'AR',
			'zip' 			=> '72113',
			'phone'			=> '555-555-5555',
			'cc_number'		=> '4111111111111111',//default fake credit card number
			'expiration_date' => date('m/').(date('Y')+1),//next year
		);
	}

	/**
	 * Validate payment form
	 */	
	function validate_payment_form($form_id, $form_values){

		/**
		 * First Name
		 */
		if (strlen($form_values['first_name']) > 255)
			form_set_error('first_name', t('The First Name field cannot be more than 255 characters long.'));
	
		/**
		 * Last Name
		 */
		if (strlen($form_values['last_name']) > 255)
			form_set_error('last_name', t('The Last Name field cannot be more than 255 characters long.'));
		
		/**
		 * Company Name
		 */
		if (strlen($form_values['company_name']) > 255)
			form_set_error('company_name', t('The Company Name field cannot be more than 255 characters long.'));
			
		/**
		 * Billing Address
		 */
		if (strlen($form_values['address1']) > 255)
			form_set_error('address1', t('The Street Address 1 field cannot be more than 255 characters long.'));

		if (strlen($form_values['address2']) > 255)
			form_set_error('address2', t('The Street Address 2 field cannot be more than 255 characters long.'));
			
		/**
		 * City
		 */
		if (strlen($form_values['city']) > 255)
			form_set_error('city', t('The Billing Address field cannot be more than 255 characters long.'));
			
		/**
		 * State
		 */
		if (strlen($form_values['state']) != 2)
			form_set_error('state', t('Invalid State selected.'));//should never trigger
			
		/**
		 * ZIP Code
		 */
		if (!preg_match('/^(\d{5}-\d{4})|(\d{5})$/', $form_values['zip']))
			form_set_error('zip', t('Invalid Zip Code entered. 5 digit and 5 digit plus 4 digit zip codes are valid.'));
			
		/**
		 * Phone
		 */
		if (!preg_match('/^(\d){3}-(\d){3}-(\d){4}/', $form_values['phone']))
			form_set_error('phone', t('Invalid Phone Number entered. xxx-xxx-xxxx is correct syntax, extensions are allowed.'));
		
		/**
		 * Credit Card Number
		 */	
		if (strlen($form_values['cc_number']) > 16)
			form_set_error('cc_number', t('The Credit Card number must be 16 numbers or less.'));
		elseif(!ctype_digit($form_values['cc_number']))
			form_set_error('cc_number', t('The Credit Card number must be digits only.'));
			
		/**
		 * Expiration Date
		 */
		if (!preg_match('/^\d{1,2}\/\d{4}$/', $form_values['expiration_date']))
			form_set_error('expiration_date', t('Invalid Expiration Date. Please enter in a 2 digit month and 4 digit year.'));
		
		/**
		 * Check for errors
		 */
		if (is_array(form_get_errors()))//fail
			return FALSE;
		else
			return TRUE;//success
	}
	
	/**
	 * Get HTML table from subscription information
	 */
	function get_subscription_info_html_table($sub){
		//price
		$rows [] = array(array('data' => 'Price: ', 'class' => 'col1'), '$'.$sub->price.' billed every ' . $sub->length . ' ' . 
					preg_replace('/s$/', '(s)', $sub->unit) . 
					($sub->total_occurrences ? $sub->total_occurrences . ' times.' : '.'));
		
		//trial amount
		if ($sub->trial_amount > 0)
			$rows [] = array(array('data' => 'Trial Amount: ', 'class' => 'col1'), '$'.$sub->trial_amount.' billed for 1st '.$sub->trial_occurrences.' occurrences of the billing cycle.');
		
		$header = array(t('Term'), t('Description'));
		$output .= theme('table', $header, $rows, NULL,'');
		
		return $output;
	}
	
	/**
	 * Get assoc array of form values for subscription group
	 */
	function get_group_form($form_values = NULL, $edit_mode = 'add'){
		$form['name'] = array(
		    '#title' => t('Name'),
			'#type' => 'textfield',
		    '#size' => 25,
		    '#maxlength' => 25,
		    '#default_value' => $form_values['name'],
		    '#required' => TRUE,
		    '#description' => 'Name the subscription group, e.g. "Book Club Membership".'
		);
		
		$form['description'] = array(
		    '#title' => t('Description'),
			'#type' => 'textarea',
		    '#default_value' => $form_values['description'],
		    '#description' => 'Describe the subscription group. This will be shown on the public subscription page.',
		    '#required' => TRUE
		);
		
		/**
		 * Cannot modify length and unit of groups once they have been created
		 */
		if ($edit_mode == 'add') {
			$form['length'] = array(
			    '#title' => t('Subscription Length'),
				'#type' => 'textfield',
			    '#size' => 3,
			    '#maxlength' => 3,
			    '#default_value' => $form_values['length'] ? $form_values['length'] : 1,//default to 1 if nothing selected
			    '#description' => 'Choose length of billing cycle, 1-12 for monthly billing, 1-365 for daily billing. Note this cannot ' .
			    				  'be changed once the group has been created.',
			    '#required' => TRUE
			);
			
			$form['unit'] = array(
			    '#type' => 'select',
			    '#title' => t('Unit'),
			    '#options' => array('days' => 'days', 'months' => 'months'),
			    '#default_value' => $form_values['unit'] ? $form_values['unit'] : 'months',//default to monthly if nothing selected
			    '#description' => 'Choose at what interval billing will occur, days or months. Note this cannot be changed once the ' .
			    				  'group has been created.',
			    '#required' => TRUE 
			);
		}
		else {//show uneditable values if edit mode
			$form['length_and_unit'] = array(
				'#type' => 'item',
				'#title' => t('Subscription Length and Unit'),
				'#value' => 'Billed every '.$form_values['length'] .' '.preg_replace('/s$/', '(s)', $form_values['unit']),//format nicer
				'#description' => 'Cannot be modified'
			);
		}
		
		$form['submit'] = array(
			'#type' => 'submit',
			'#value' => $edit_mode == 'add' ? t('Add Subscription Group') : t('Update Subscription Group'),
		);
		
		return $form;
	}
	
	/**
	 * Get grouped hash of role subscription data for display in radio options
	 * 
	 * @return array role subscription group names as the hash with the role subscription array being the value for each key
	 */
	function group_subscription_radio_options(){
		$result = Role_subscription_db::find_all();//get db result
		
		while ($row = db_fetch_object($result)) 
			$groups [$row->group_name][$row->id] = '<span>'.$row->title.'</span><div class="role_subscription_radio_options">'.$row->description.'</div>';
		
		return $groups;
	}
}