<?php

/**
 * @file
 * Handles the database functions for managing role subscription history
 */

/**
 * Functions for managing role subscription history 
 */
class Role_subscription_history_db {
	
	/**
	 * Add role subscription payment version for history
	 * 
	 * @param array $form_values
	 * @param integer $role_subscription_id
	 * @param string $status defaults to 'active'
	 * 
	 * @return mixed a database query result resource, or FALSE if the query was not executed correctly.
	 */
	function add_version($form_values, $role_subscription_id, $status = 'active'){
		$f = $form_values;//shortcut var
		
		$query = "insert into {role_subscription_history} " .
				 "(role_subscription_id, title, price, trial_amount, total_occurrences, trial_occurrences, " .
				 "description, weight, status) " .
			     "values ('%d', '%s', '%s', '%s', '%d', '%d', '%s', '%d', '%s');";
			   
		return db_query($query, $role_subscription_id, $f['title'], $f['price'], $f['trial_amount'], $f['total_occurrences'], 
								$f['trial_occurrences'], $f['description'], $f['weight'], $status);//will return 1 if success
	}
	
	/**
	 * Find version ID
	 * 
	 * @param integer $vid
	 * 
	 * @return object role subscription version data
	 */
	function find($vid){
		$query = "select rsh.* from {role_subscription_history} rph where rsh.vid = %d; ";
			   
		return db_fetch_object(db_query($query, $vid));
	}
	
	/**
	 * Find history for role subscription
	 * 
	 * @param integer $role_subscription_id
	 * 
	 * @return mixed a database query result resource, or FALSE if the query was not executed correctly. 
	 */
	function find_all($role_subscription_id){
		if (!$role_subscription_id) return;//fail
		
		$query = "select rsh.*, rs.id, rs.rid, rs.role_subscription_group_id " .
				 "from {role_subscription_history} rsh " .
				 "join {role_subscription} rs on rsh.role_subscription_id = rs.id " .
				 "where rsh.role_subscription_id = %d order by created_at desc; ";
			   
		return db_query($query, $role_subscription_id);
	}
}