<?php

/**
 * @file
 * Handles the commoner functions for role subscription that don't involve db or forms directly
 */
 
/**
 * Common functions for role_subscription module
 */
class Role_subscription {
	
	/**
	 * Generate 9 digit invoice number with timestamp
	 * 
	 * @return string 9 digit invoice number that includes timestamp and 3 digit random number appended
	 */
	function generate_invoice_number(){
		return date('Ymdhis').rand(111,999);
	}
	
	/**
	 * Add a user role
	 * 
	 * @param string $role name of role 
	 * @param integer $uid user ID
	 * 
	 * @todo how do you tell if the user_save function fails? it always returns the user object
	 * 
	 * @return object user object
	 */
	function add_user_role($role, $uid){
		/**
		 * Find role ID for this role name (drupal is picky ab rid in user role list)
		 */
		$rid = self::_get_rid_for_role($role);
		
		$user = user_load(array('uid' => $uid));

		$user_roles = $user->roles;
		
		if (!in_array($role, $user_roles))//don't add twice
			$user_roles[$rid]  = $role;
		
		return module_invoke('user', 'save', $user, array('roles' => $user_roles));
	}
	
	/**
	 * Get role ID for this role name
	 * 
	 * @param string $role name of role
	 * @return integer role ID
	 */
	function _get_rid_for_role($role){
		return db_fetch_object(db_query('select rid from {role} where name = "%s"', $role))->rid;
	}
	
	/**
	 * Remove a user role
	 * 
	 * @param string $role name of role 
	 * @param integer $uid user ID
	 * 
	 * @todo how do you know if user_save function fails?
	 * 
	 * @return mixed user object or TRUE if no role to remove
	 */
	function remove_user_role($role, $uid) {
		$user = user_load(array('uid' => $uid));
		$user_roles = $user->roles;
		
		/**
		 * Look for role
		 */
		if (in_array($role, $user_roles)) {
			/**
			 * Remove role from user list of roles
			 */
			foreach ($user_roles as $key => $val)
				if ($val == $role)//find role
					unset($user_roles[$key]);//remove from user role array
					
			return module_invoke('user', 'save', $user, array('roles' => $user_roles));
		}
		else
			return TRUE;//no role to remove
	}
}