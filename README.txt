
role_subscription for Drupal
====================

The role_subscription module makes it simple to manage paid subscriptions and tie subscriptions
to user roles in drupal. Brought to you by http://www.Lynxmark.com and http://jay.callicott.name/


INSTALLATION
------------

1. Be sure to enable ALL Role Subscription modules, the module will not function otherwise


CONFIGURATION
-------------
   
1. Enable role_subscription modules in:
       admin/build/modules
   
2. Settings - be sure you configure the payment engine first
	   admin/settings/role_subscription
	   
3. Secure pages settings:
	   admin/settings/securepages - enable SSL and add the pattern "role_subscription*"
	   to the pages box. This will redirect all the public and user role subscription
	   pages https pages. Also I would recommend leaving the defaults including the admin
	   pages. Lastly I recommend checking "Switch back to http pages when there are no matches"
	   for better performance.